import { useController } from 'react-hook-form';

export const Input = (props: { name: string, control: any }) => {
  const {
    field,
    fieldState: { invalid, isTouched, isDirty },
  } = useController({
    name: props.name,
    control: props.control,
  })

  return (
    <div>
      <input
        {...field}
      />
      <details>
        <pre>
isDirty: {`${isDirty}`}<br/>
isTouched: {`${isTouched}`}<br/>
isValid: {`${!invalid}`}<br/>
        </pre>
      </details>
    </div>
  );
}
