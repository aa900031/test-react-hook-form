import { useState, useCallback } from 'react'
import { useFieldArray, useForm } from 'react-hook-form'
import { Input } from './FieldInput'

function App() {
  const { handleSubmit, control } = useForm({
    defaultValues: {
      users: Array.from({ length: 1000 }).map((val, index) => ({
        name: `user${index}`,
        age: index,
      }))
    }
  });
  const onSubmit = (data: any) => console.log(data);
  const { fields, append, prepend, remove } = useFieldArray({
    name: 'users',
    control,
  })

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div>
        {fields.map((field, index) => (
          <div
            key={field.id}
          >
            <Input
              name={`users.${index}.name`}
              control={control}
            />
            <Input
              name={`users.${index}.age`}
              control={control}
            />
            <button
              type="button"
              onClick={() => remove(index)}
            >
              Remove
            </button>
          </div>
        ))}
        <button
          type="button"
          onClick={() =>
            append({
              name: 'new',
              age: undefined,
            })
          }
        >
          Append
        </button>
        <button
          type="button"
          onClick={() =>
            prepend({
              name: 'new',
              age: 0,
            })
          }
        >
          Prepend
        </button>
      </div>
      <input type="submit" />
    </form>
  );
}

export default App
